//
//  QuickbloxHelper.swift
//  uberbiz
//
//  Created by Filbert Hartawan on 03/02/21.
//

import Foundation
import Quickblox
import RxSwift
import RxCocoa

struct QBCustomImage {
    var imageUrl: String = ""
    var height: CGFloat = 0
    var width: CGFloat = 0
}

struct QBCustomDialog{
    var role: String = UserRole.BUYER
    var dialogs: [QBChatDialog] = []
}

class QuickbloxHelper{
    static let shared = QuickbloxHelper()
    
    var currentUserId: UInt = 0
    
    let isSuccess:PublishSubject<Bool> = PublishSubject()
    let isConnectSuccess:PublishSubject<Bool> = PublishSubject()
    let isDisconnectSuccess:PublishSubject<Bool> = PublishSubject()
    let isLogoutSuccess:PublishSubject<Bool> = PublishSubject()
    let isSendSuccess:PublishSubject<Bool> = PublishSubject()
    
    let dialog:PublishSubject<QBChatDialog> = PublishSubject()
    let customDialogs:PublishSubject<QBCustomDialog> = PublishSubject()
    let user:PublishSubject<QBUUser> = PublishSubject()
    let users:PublishSubject<[QBUUser]> = PublishSubject()
    let occupantUsers:PublishSubject<[QBUUser]> = PublishSubject()
    let messages:PublishSubject<[QBChatMessage]> = PublishSubject()
    
    let qbCustomImage:PublishSubject<QBCustomImage> = PublishSubject()
        
    let errorMsg:PublishSubject<String> = PublishSubject()
    let connectErrorMsg:PublishSubject<String> = PublishSubject()

    private var userLogin = ""
    
    var qbDialog:QBChatDialog?
    
    func login(user:User, type:String = UserRole.BUYER){
        if type == UserRole.BUYER{
            if let username = user.username {
                self.userLogin = username
            }
        }else{
            if let storeName = user.storeName{
                self.userLogin = storeName.replacingOccurrences(of: " ", with: "_")
            }else{
                self.errorMsg.onNext("Username/Store name missing.")
            }
        }
                
        QBRequest.logIn(withUserLogin: self.userLogin, password: self.userLogin + "_1234", successBlock: { (response, user) in
            if self.userLogin == user.login{
                self.currentUserId = user.id
                UserDefaultHelper.shared.setupQBUser(userId: String(user.id))
                self.isSuccess.onNext(true)
            }
        }, errorBlock: { (response) in
            print("RESPONSE: \(response)")

            if response.status == QBResponseStatusCode.unAuthorized{
                self.register(user: user, type: type)
            }else{
                if let error = response.error{
                    if let reasons = error.reasons{
                        if let messages = reasons["errors"] as? NSArray{
                            self.errorMsg.onNext(messages.firstObject as! String)
                        }
                    }else{
                        self.errorMsg.onNext("Sorry, there was an error with the server.")
                    }
                }else{
                    self.errorMsg.onNext("Sorry, there was an error with the server.")
                }
            }
        })
    }
    
    func register(user:User, type:String){
        
        let QBUser = QBUUser()
        QBUser.login = self.userLogin
        QBUser.fullName = self.userLogin
        QBUser.password = self.userLogin + "_1234"

        QBRequest.signUp(QBUser, successBlock: { response, QBUser in
            self.login(user: user, type: type)
        }, errorBlock: { (response) in
            print("RESPONSE: \(response)")
            
            if let error = response.error{
                if let reasons = error.reasons{
                    if let messages = reasons["errors"] as? NSArray{
                        self.errorMsg.onNext(messages.firstObject as! String)
                    }
                }else{
                    self.errorMsg.onNext("Sorry, there was an error with the server.")
                }
            }else{
                self.errorMsg.onNext("Sorry, there was an error with the server.")
            }
        })
    }
    
    func logout(){
        QBRequest.logOut(successBlock: { (response) in
            self.isLogoutSuccess.onNext(true)
        }, errorBlock: { (response) in
            print("RESPONSE: \(response)")
            
            if let error = response.error{
                if let reasons = error.reasons{
                    if let messages = reasons["errors"] as? NSArray{
                        self.errorMsg.onNext(messages.firstObject as! String)
                    }
                }else{
                    self.errorMsg.onNext("Sorry, there was an error with the server.")
                }
            }else{
                self.errorMsg.onNext("Sorry, there was an error with the server.")
            }
        })
    }
    
    func connect(type:String){
        if QBChat.instance.isConnected || QBChat.instance.isConnecting{
            DispatchQueue.main.async {
                self.isConnectSuccess.onNext(true)
            }
        }else{
            if let qbUser = UserDefaultHelper.shared.getQBUser(), let qbUserId = UInt(qbUser), let user = UserDefaultHelper.shared.getUser(){
                var password = ""
                if type == Role.BUYER{
                    password = user.username! + "_1234"
                }else{
                    password = user.storeName!.replacingOccurrences(of: " ", with: "_") + "_1234"
                }
                QBChat.instance.connect(withUserID: qbUserId, password: password) { (error) in
                    if let error = error{
                        print(error)
                        self.connectErrorMsg.onNext(error.localizedDescription)
                    }else{
                        self.isConnectSuccess.onNext(true)
                    }
                }
            }
        }
    }
    
    func disconnect(){
        if QBChat.instance.isConnected{
            QBChat.instance.disconnect { (error) in
                if let error = error{
                    self.errorMsg.onNext(error.localizedDescription)
                }else{
                    self.isDisconnectSuccess.onNext(true)
                }
            }
        }else{
            DispatchQueue.main.async {
                self.isDisconnectSuccess.onNext(true)
            }
        }
    }
    
    func getUsers(userLogins:[String]){
        let page = QBGeneralResponsePage(currentPage: 0, perPage: UInt(userLogins.count))
        QBRequest.users(withLogins: userLogins, page: page, successBlock: { (response, page, users) in
            self.users.onNext(users)
        }, errorBlock:{ (response) in
            print("RESPONSE: \(response)")
            
            if let error = response.error{
                if let reasons = error.reasons{
                    if let messages = reasons["errors"] as? NSArray{
                        self.errorMsg.onNext(messages.firstObject as! String)
                    }
                }else{
                    self.errorMsg.onNext("Sorry, there was an error with the server.")
                }
            }else{
                self.errorMsg.onNext("Sorry, there was an error with the server.")
            }
        })
    }
    
    func getAllDialogs(role: String){
        QBRequest.dialogs(for: QBResponsePage(limit: 50, skip: 0), extendedRequest: nil, successBlock: { (response, dialogs, dialogsUsersIDs, page) in
            
            var customDialogs = QBCustomDialog()
            customDialogs.role = role
            customDialogs.dialogs = dialogs
            self.customDialogs.onNext(customDialogs)
            
        }, errorBlock: { (response) in
            print("RESPONSE: \(response)")
            
            if let error = response.error{
                if let reasons = error.reasons{
                    if let messages = reasons["errors"] as? NSArray{
                        self.errorMsg.onNext(messages.firstObject as! String)
                    }
                }else{
                    self.errorMsg.onNext("Sorry, there was an error with the server.")
                }
            }else{
                self.errorMsg.onNext("Sorry, there was an error with the server.")
            }
        })
    }
    
    func getAllChat(dialog: QBChatDialog, limit: Int, skip: Int){
        let page = QBResponsePage(limit: limit, skip: skip)
        let extendedRequest = ["sort_asc": "date_sent", "mark_as_read": "0"]
        QBRequest.messages(withDialogID: dialog.id!, extendedRequest: extendedRequest, for: page, successBlock: { (response, messages, page) in
            self.messages.onNext(messages)
        }, errorBlock: { (response) in
            print("RESPONSE: \(response)")
            if let error = response.error{
                if let reasons = error.reasons{
                    if let messages = reasons["errors"] as? NSArray{
                        self.errorMsg.onNext(messages.firstObject as! String)
                    }
                }else{
                    self.errorMsg.onNext("Sorry, there was an error with the server.")
                }
            }else{
                self.errorMsg.onNext("Sorry, there was an error with the server.")
            }
        })
    }
    
    func getUsers(occupantId:[String]){
        QBRequest.users(withIDs: occupantId, page: nil, successBlock: { (response, page, users) in
            self.occupantUsers.onNext(users)
        }, errorBlock:{ (response) in
            print("RESPONSE: \(response)")
            if let error = response.error{
                if let reasons = error.reasons{
                    if let messages = reasons["errors"] as? NSArray{
                        self.errorMsg.onNext(messages.firstObject as! String)
                    }
                }else{
                    self.errorMsg.onNext("Sorry, there was an error with the server.")
                }
            }else{
                self.errorMsg.onNext("Sorry, there was an error with the server.")
            }
        })
    }
    
    func createDialog(occupantIds:[NSNumber], user: User?){
        let dialog = QBChatDialog(dialogID: nil, type: .group)
        dialog.name = String(format: "{sender:%@, receiver:%@}", UserDefaultHelper.shared.getUser()!.username!, user?.storeName ?? "null")
        if let user = user{
            // MARK: Create photo for create request quotation
            dialog.photo = String(format: "%@|%@", user.storeImageUrl ?? "", UserDefaultHelper.shared.getUser()!.storeImageUrl ?? "")
        }
        
        var occupantIdsFinal:[NSNumber] = []
        // MARK: Append Admin QBId for admin organizer
        occupantIdsFinal.append(126022922)
        occupantIdsFinal.append(NSNumber(value: self.currentUserId))
        occupantIdsFinal.append(contentsOf: occupantIds)
                
        dialog.occupantIDs = occupantIdsFinal
                
        QBRequest.createDialog(dialog, successBlock: { (response, dialog) in
            self.dialog.onNext(dialog)
        }, errorBlock: { (response) in
            print("RESPONSE: \(response)")
            if let error = response.error{
                if let reasons = error.reasons{
                    if let messages = reasons["errors"] as? NSArray{
                        self.errorMsg.onNext(messages.firstObject as! String)
                    }
                }else{
                    self.errorMsg.onNext("Sorry, there was an error with the server.")
                }
            }else{
                self.errorMsg.onNext("Sorry, there was an error with the server.")
            }
        })
    }
    
    func sendChat(message: QBChatMessage, dialog:QBChatDialog){
        dialog.join { (error) in
            if let error = error{
                self.errorMsg.onNext(error.localizedDescription)
            }else{
                dialog.send(message) { (error) in
                    if let error = error{
                        print(error)
                        self.errorMsg.onNext(error.localizedDescription)
                    }else{
                        self.isSendSuccess.onNext(true)
                    }
                }
            }
        }
    }
    
    func uploadImage(image: UIImage, fileName:String){
        guard let imageData = image.jpegData(compressionQuality: 0.5) else {
            return
        }
        
        QBRequest.tUploadFile(imageData, fileName: fileName, contentType: "image/jpeg", isPublic: true, successBlock: { (response, uploadedBlob) in
            print(response)
            if let imageUrl  = QBCBlob.publicUrl(forFileUID: uploadedBlob.uid!){
                var qbCustomImage = QBCustomImage()
                qbCustomImage.imageUrl = imageUrl
                qbCustomImage.height = image.size.height * image.scale
                qbCustomImage.width = image.size.width * image.scale
                
                self.qbCustomImage.onNext(qbCustomImage)
            }else{
                self.errorMsg.onNext("Image url is missing")
            }
        }, statusBlock: { (request, status)  in
            
        }, errorBlock: { (response) in
            print("RESPONSE: \(response)")
            if let error = response.error{
                if let reasons = error.reasons{
                    if let messages = reasons["errors"] as? NSArray{
                        self.errorMsg.onNext(messages.firstObject as! String)
                    }
                }else{
                    self.errorMsg.onNext("Sorry, there was an error with the server.")
                }
            }else{
                self.errorMsg.onNext("Sorry, there was an error with the server.")
            }
        })
    }
}
