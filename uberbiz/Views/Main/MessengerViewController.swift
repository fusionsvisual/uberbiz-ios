//
//  MessengerViewController.swift
//  uberbiz
//
//  Created by Filbert Hartawan on 04/01/21.
//

import UIKit
import RxSwift
import RxCocoa
import Quickblox
import SVProgressHUD

struct CustomDialog {
    var dialog:QBChatDialog?
    var user: QBUUser?
}

protocol MessengerProtocol {
    func didSelect(index:Int)
}

class MessengerViewController: UIViewController {

//    @IBOutlet var messengerTableView: UITableView!
    @IBOutlet var messengerCV: UICollectionView!
    @IBOutlet var horizontalBarLeft: NSLayoutConstraint!
    
    @IBOutlet var tabLayoutLabels: [UILabel]!
    
    private var selectedIndex = 0
    private var isDataLoaded = false
    private var qbDialogs:[QBChatDialog] = []
    private var users:[QBUUser] = []
    private var disposeBag:DisposeBag = DisposeBag()
    
    private var selectedRole:String = UserRole.BUYER
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupData(index: self.selectedIndex)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.disposeBag = DisposeBag()
    }
    
    private func setupViews(){
        self.messengerCV.register(UINib(nibName: "MessengerSectionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MessengerSectionCell")
    }
    
    private func setupData(index: Int){
        self.disposeBag = DisposeBag()
        self.observeViewModel()
                
        self.isDataLoaded = false
        self.qbDialogs.removeAll()
        self.messengerCV.reloadData()
        
        self.selectedIndex = index
            
        if self.selectedIndex == 0{
            self.selectedRole = UserRole.BUYER
            if let user = UserDefaultHelper.shared.getUser(){
                if user.username != nil{
                    QuickbloxHelper.shared.login(user: UserDefaultHelper.shared.getUser()!, type: UserRole.BUYER)
                }else{
                    self.isDataLoaded = true
                }
            }
        }else{
            self.selectedRole = UserRole.SELLER
            if let user = UserDefaultHelper.shared.getUser(){
                if user.storeName != nil{
                    QuickbloxHelper.shared.login(user: UserDefaultHelper.shared.getUser()!, type: UserRole.SELLER)
                }else{
                    self.isDataLoaded = true
                }
            }
        }
        
        self.messengerCV.reloadData()
    }
    
    private func observeViewModel(){
        QuickbloxHelper.shared.isSuccess.bind { (isSuccess) in
            QuickbloxHelper.shared.connect(type: self.selectedRole)
        }.disposed(by: disposeBag)
        
        QuickbloxHelper.shared.isConnectSuccess.bind { (isConnectSuccess) in
            QuickbloxHelper.shared.getAllDialogs(role: self.selectedRole)
        }.disposed(by: disposeBag)
        
        QuickbloxHelper.shared.customDialogs.bind { (customDialogs) in
            
            if self.selectedRole == customDialogs.role{
                self.qbDialogs.removeAll()

                for qbDialog in customDialogs.dialogs{
                    if self.selectedRole == UserRole.BUYER{
                        if String(qbDialog.userID) == UserDefaultHelper.shared.getQBUser()!{
                            self.qbDialogs.append(qbDialog)
                        }
                    }else{
                        if String(qbDialog.userID) != UserDefaultHelper.shared.getQBUser()!{
                            self.qbDialogs.append(qbDialog)
                        }
                    }
                }
                self.isDataLoaded = true
                self.messengerCV.reloadData()
            }
            
        }.disposed(by: disposeBag)
        
        QuickbloxHelper.shared.connectErrorMsg.bind { (errorMsg) in
            SVProgressHUD.showError(withStatus: errorMsg)
            SVProgressHUD.dismiss(withDelay: Delay.SHORT)
        }.disposed(by: disposeBag)
    }
    
    private func clearData(){
        self.isDataLoaded = false
        self.qbDialogs.removeAll()
        self.messengerCV.reloadData()
    }
    
    private func setupTabLayout(selectedIdx:Int){
        for (index, element) in self.tabLayoutLabels.enumerated() {
            if (index == selectedIdx){
                element.textColor = .black
            }else{
                element.textColor = Color.TABBAR_INACTIVE_COLOR
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.horizontalBarLeft.constant = scrollView.contentOffset.x/2
        self.clearData()
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x/self.view.frame.width)
        self.setupData(index: index)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x/self.view.frame.width)
        self.setupTabLayout(selectedIdx: index)
        
        self.setupData(index: index)
    }

    @IBAction func didTapLayoutTapped(_ sender: UIButton) {
        self.setupTabLayout(selectedIdx: sender.tag)
        self.messengerCV.scrollToItem(at: IndexPath(item: sender.tag, section: 0), at: .left, animated: true)
        
        self.clearData()
//        self.setupData(index: sender.tag)
    }
}

extension MessengerViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.messengerCV.frame.width, height: self.messengerCV.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MessengerSectionCell", for: indexPath) as! MessengerSectionCollectionViewCell
        cell.delegate = self
        cell.isDataLoaded = self.isDataLoaded
        cell.qbDialogs = self.qbDialogs
                
//        if self.qbDialogs.count <= 0 {
//            cell.emptyStateL.isHidden = false
//        }else{
//            cell.emptyStateL.isHidden = true
//        }
        
        return cell
    }
}

extension MessengerViewController: MessengerProtocol{
    func didSelect(index: Int) {
        let chatVC = ChatViewController()
        chatVC.userRole = self.selectedRole
        chatVC.qbDialog = self.qbDialogs[index]
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
}
