//
//  MessengerSectionCollectionViewCell.swift
//  uberbiz
//
//  Created by Filbert Hartawan on 18/02/21.
//

import UIKit
import RxCocoa
import RxSwift
import Quickblox

class MessengerSectionCollectionViewCell: UICollectionViewCell {

    @IBOutlet var messengerTableV: UITableView!
    @IBOutlet var emptyStateL: UILabel!
    
    var qbDialogs:[QBChatDialog] = [] {
        didSet{
            if self.qbDialogs.count == 0 && self.isDataLoaded{
                self.emptyStateL.isHidden = false
            }else{
                self.emptyStateL.isHidden = true
            }
            self.messengerTableV.reloadData()
        }
    }
    var isDataLoaded = false
    
    var delegate: MessengerProtocol?
    
    private var disposeBag:DisposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.messengerTableV.register(UINib(nibName: "ShimmerMyProductTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerCell")
        self.messengerTableV.register(UINib(nibName: "MessengerTableViewCell", bundle: nil), forCellReuseIdentifier: "MessengerCell")
        
        self.messengerTableV.delegate = self
        self.messengerTableV.dataSource = self
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.emptyStateL.isHidden = true
    }

}

extension MessengerSectionCollectionViewCell: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isDataLoaded{
            return self.qbDialogs.count
        }else{
            return 10
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isDataLoaded{
            self.delegate?.didSelect(index: indexPath.item)
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isDataLoaded{

            let cell = tableView.dequeueReusableCell(withIdentifier: "MessengerCell", for: indexPath) as! MessengerTableViewCell

            if let name = self.qbDialogs[indexPath.item].name{
                cell.nameL.text = name
            }

            if let imageUrlString = self.qbDialogs[indexPath.item].photo{
                if let imageUrl = URL(string: imageUrlString){
                    cell.messengerImageView.af.setImage(withURL: imageUrl, placeholderImage: UIImage(named: "img_placeholder"))
                }
            }

            if let lastMessageText = self.qbDialogs[indexPath.item].lastMessageText{
                cell.messageL.text = lastMessageText
            }else{
                cell.messageL.text = "Start Chat Now!"
            }

            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerCell", for: indexPath)
            return cell
        }
    }
}
