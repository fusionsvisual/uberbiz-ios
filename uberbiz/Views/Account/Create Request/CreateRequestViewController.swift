//
//  CreateRequestViewController.swift
//  uberbiz
//
//  Created by Filbert Hartawan on 10/01/21.
//

import UIKit
import SVProgressHUD

struct CreateRequestItem {
    static var selectedUnit:Unit?
    static var selectedCategory:SubCategory?
}

protocol CreateRequestDelegate{
    func didSelectAddress(address: Address)
}

class CreateRequestViewController: UIViewController {

    @IBOutlet var titleTF: UITextField!
    @IBOutlet var descriptionTF: UITextField!
    @IBOutlet var quantityTF: UITextField!
    
    @IBOutlet var categoryNameL: UILabel!
    @IBOutlet var unitNameL: UILabel!
    @IBOutlet var addressL: UILabel!
    @IBOutlet var addAddressL: UILabel!
    
    @IBOutlet var addressV: UIView!
    
    private var selectedAddress: Address?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let subcategory = CreateRequestItem.selectedCategory{
            self.categoryNameL.text = subcategory.subCategoryName
        }
        
        if let unit = CreateRequestItem.selectedUnit{
            self.unitNameL.text = unit.unitName
        }
        
        if let address = self.selectedAddress{
            self.addressL.text = address.addressTitle
            self.addAddressL.isHidden = true
            self.addressV.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if self.isMovingFromParent{
            CreateRequestItem.selectedUnit = nil
            CreateRequestItem.selectedCategory = nil
        }
    }

    private func setupViews(){
        self.title = "Create Request"
    }
    
    private func isValid()->Bool{
        if self.titleTF.text == nil{
            return false
        }
        
        if self.descriptionTF.text == nil{
            return false
        }
        
        if self.quantityTF.text == nil{
            return false
        }
        
        if CreateRequestItem.selectedCategory == nil{
            return false
        }
        
        if CreateRequestItem.selectedUnit == nil{
            return false
        }
        
        if self.selectedAddress == nil{
            return false
        }
        
        return true
    }
    
    @IBAction func categoryAction(_ sender: Any) {
        let categoryListVC = AddProductCategoryListViewController()
        categoryListVC.type = CategoryVCSource.REQUEST
        self.navigationController?.pushViewController(categoryListVC, animated: true)
    }
    
    @IBAction func unitAction(_ sender: Any) {
        let unitListVC = AddProductUnitViewController()
        unitListVC.type = UnitVCSource.REQUEST
        self.navigationController?.pushViewController(unitListVC, animated: true)
    }
    
    @IBAction func addAddressAction(_ sender: Any) {
        let addAddressVC = AddAddressViewController()
        addAddressVC.currentAddress = self.selectedAddress
        addAddressVC.sourceVC = AddAddressSource.REQUEST
        addAddressVC.createRequestDelegate = self
        self.navigationController?.pushViewController(addAddressVC, animated: true)
    }
    
    @IBAction func proceedAction(_ sender: Any) {
        
        if self.isValid(){
            var requestQuotation = RequestQuotation()
            requestQuotation.title = self.titleTF.text!
            requestQuotation.description = self.descriptionTF.text!
            requestQuotation.subcategory = CreateRequestItem.selectedCategory
            requestQuotation.unit = CreateRequestItem.selectedUnit
            requestQuotation.quantity = Int(self.quantityTF.text!)!
            requestQuotation.deliveryAddress = self.selectedAddress
            
            let companyListVC = CompanyTargetListViewController()
            companyListVC.requestQuotation = requestQuotation
            self.navigationController?.pushViewController(companyListVC, animated: true)
        }else{
            SVProgressHUD.showError(withStatus: Message.FILL_THE_BLANKS)
            SVProgressHUD.dismiss(withDelay: Delay.SHORT)
        }
    }
}

extension CreateRequestViewController: CreateRequestDelegate{
    func didSelectAddress(address: Address) {
        self.selectedAddress = address
    }
}
